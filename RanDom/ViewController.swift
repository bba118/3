//
//  ViewController.swift
//  RanDom
//
//  Created by ZCS on 27/11/2014.
//  Copyright (c) 2014 ZCS. All rights reserved.
//

import UIKit
import Social


class ViewController: UIViewController {

    @IBOutlet weak var randomNumber: UILabel!

    @IBOutlet weak var pageIndicator: UIPageControl!
    @IBOutlet var left: UISwipeGestureRecognizer!
    @IBOutlet var right: UISwipeGestureRecognizer!

    
    @IBAction func swipeLeft(sender: UISwipeGestureRecognizer) {
        println("swipeLeft")
        
        let quoteUrl = "http://www.iheartquotes.com/api/v1/random?format=json&max_lines=2&max_characters=80"
        
        let session = NSURLSession.sharedSession()
        session.dataTaskWithURL(NSURL(string: quoteUrl)!, completionHandler: {
            (data, response, error) in
            if error != nil {
                println("Error retrieveing quote: \(error.localizedDescription)")
                return
            }
            var errorJSON: NSError?
            let quoteAny: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: .allZeros, error: &errorJSON)
            if errorJSON != nil {
                println("Error parsing quote: \(errorJSON!.localizedDescription)")
                return
            }
            
            if let quoteJSON = quoteAny as? [String: AnyObject] {
                
                let quote = quoteJSON["quote"] as String
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.randomNumber.text = quote
                })
            }
        }).resume()
    }
    
    @IBAction func swipeRight(sender: UISwipeGestureRecognizer) {
        println("swipeRight")
        
        let quoteUrl = "http://www.iheartquotes.com/api/v1/random?format=json&max_lines=2&max_characters=80"

        let session = NSURLSession.sharedSession()
        session.dataTaskWithURL(NSURL(string: quoteUrl)!, completionHandler: {
            (data, response, error) in
            if error != nil {
                println("Error retrieveing quote: \(error.localizedDescription)")
                return
            }
            var errorJSON: NSError?
            let quoteAny: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: .allZeros, error: &errorJSON)
            if errorJSON != nil {
                println("Error parsing quote: \(errorJSON!.localizedDescription)")
                return
            }
            
            if let quoteJSON = quoteAny as? [String: AnyObject] {
                
                let quote = quoteJSON["quote"] as String
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.randomNumber.text = quote
                })
            }
        }).resume()
    }
    
    @IBAction func shareToFacebook() {
        
        var shareToFacebook : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        
        // display the quote to on the text field.
        shareToFacebook.setInitialText(self.randomNumber.text)
        
        
        self.presentViewController(shareToFacebook, animated: true, completion: nil)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.addGestureRecognizer(self.left)
        self.view.addGestureRecognizer(self.right)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

