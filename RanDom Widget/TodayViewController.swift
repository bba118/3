//
//  TodayViewController.swift
//  RanDom Widget
//
//  Created by ZSC on 27/11/2014.
//  Copyright (c) 2014 ZCS. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var randomNumber: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)!) {
        // Perform any setup necessary in order to update the view.

        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        if let sharedDefaults = NSUserDefaults(suiteName: "group.uk.ac.coventry.mtyers.randomwidget") {
            let num = sharedDefaults.integerForKey("randomNumber")
            //self.randomNumber.text = "\(quote)"
        }
        
        completionHandler(NCUpdateResult.NewData)
    }
    
}
